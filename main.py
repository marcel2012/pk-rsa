# correct key suffix: 473539

def decrypt_rsa_message(n, m_k, d, iterations):
    import os
    rank = int(os.environ['RANK'])
    ranks = int(os.environ['RANKS'])
    for priv_key_suffix in range(rank, iterations, ranks):
        if priv_key_suffix % 100000 < ranks:
            print('progress:', str(priv_key_suffix), '/', str(iterations))
        plaintext = pow(m_k, d + priv_key_suffix, n).to_bytes(64, byteorder='big')
        if plaintext[0] == 0x00 and plaintext[1] == 0x02 and plaintext[-6] == 0x00:
            message = plaintext[-5:].decode()
            return priv_key_suffix, message
    return None, None


if __name__ == '__main__':
    priv_key_suffix, message = decrypt_rsa_message(
        0xa357a8699579f2d4ad03cd4b5ffe6d1c883e663fddb96f571641ef9ab5e73bc2f1a1c2615f0e606912ae16e0eca0f71b1dd4f36c74045caf70a1518795f44eed,
        0x9142a4a0c806916cdc1b3b68659da107357b5abf7b1fbbc1853e94ce2f3841e12ea9d5e73354c5e5771afcf524f5a8e918ed3b8096d41d8b4ce60319e2570cf8,
        0x19f575b2278400479ea52d30529c9c155845fdc1b67eeba77e7d0fc6e85d7ab0a87888d4d3ca4f0005150e2678f41011d6b9bee0a53f14fabdf43fb83bb00000,
        0x100000)
    print('Key suffix:', priv_key_suffix)
    print('Message:', message)
