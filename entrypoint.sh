#!/bin/sh
set -eux

RANKS=31
for RANK in `seq 2 $RANKS`;
do
  RANK=$RANK RANKS=$RANKS python3 main.py &
done;
RANK=1 RANKS=$RANKS python3 main.py
