FROM alpine:latest

RUN apk add --update --no-cache gcc musl-dev python3

WORKDIR /app

COPY main.py entrypoint.sh ./

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]